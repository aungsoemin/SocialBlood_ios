//
//  HomeViewController.m
//  SocialBlood
//
//  Created by Nex Technology on 5/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "HomeViewController.h"
#import "StringTable.h"

@interface HomeViewController (){
    int bloodType;
    int bloodRHTYPE;
}

@property (nonatomic, strong) NSMutableIndexSet *selectedIndexes;
@property (nonatomic, strong) NSMutableIndexSet *selectedBloodTypeIndexes;

@end

@implementation HomeViewController
@synthesize lbl_bloodType;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self cardSetup];
    
        // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    lbl_bloodType.text=[[self list]objectAtIndex:3];
    lbl_bloodType.textColor=[UIColor whiteColor];
    lbl_bloodType.textAlignment=NSTextAlignmentCenter;
    lbl_bloodType.backgroundColor=[UIColor colorWithHexString:@"5a47a0"];
    lbl_bloodType.layer.cornerRadius=50.0;
    lbl_bloodType.clipsToBounds=YES;
  
}

- (void)cardSetup
{
    self.cardView.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.cardView.layer.shadowOffset = CGSizeMake(1.f,1.f);
    self.cardView.layer.shadowRadius = 3.f;
    self.cardView.layer.shadowOpacity = .5f;
    self.cardView.layer.masksToBounds = NO;
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickEnterLocation:(id)sender {
    LocationViewController * locationVC = [[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateViewControllerWithIdentifier:@"LocationVC"];
   
    [self presentViewController:locationVC animated:YES completion:^{
        
    }];
    
}

- (IBAction)onClickSelectBlood:(id)sender {
    
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    
    CGPoint point = CGPointMake(paddingLeftRight,self.view.frame.size.height/2-100);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)),220);
    
    LPPopupListView *listView = [[LPPopupListView alloc] initWithTitle:@"Select the blood type" list:[self list] selectedIndexes:self.selectedIndexes point:point size:size selectedTab:BLOODTYPE  multipleSelection:NO disableBackgroundInteraction:NO];
    listView.tag=BLOODTYPE;
    listView.delegate = self;
    [listView showInView:self.view animated:YES];
    
}

- (void)popupListView:(LPPopupListView *)popUpListView didSelectIndex:(NSInteger)index
{
    
    bloodType=index;
    float paddingTopBottom = 20.0f;
    float paddingLeftRight = 20.0f;
    CGPoint point = CGPointMake(paddingLeftRight,self.view.frame.size.height/2-100);
    CGSize size = CGSizeMake((self.view.frame.size.width - (paddingLeftRight * 2)),170);
    LPPopupListView *listView = [[LPPopupListView alloc] initWithTitle:@"Choose Blood RH Type" list:[self bloodtype] selectedIndexes:self.selectedBloodTypeIndexes  point:point size:size selectedTab:BLOODRHTYPE  multipleSelection:NO disableBackgroundInteraction:NO];
    listView.delegate = self;
    listView.tag=BLOODRHTYPE;
    [listView showInView:self.view animated:YES];
 
}

- (void)popupListViewDidHide:(LPPopupListView *)popUpListView selectedIndexes:(NSIndexSet *)selectedIndexes
{
    NSLog(@"popupListViewDidHide - selectedIndexes: %@", selectedIndexes.description);
    

    self.selectedIndexes = [[NSMutableIndexSet alloc] initWithIndexSet:selectedIndexes];
    
    lbl_bloodType.text = @"";
    [selectedIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        bloodRHTYPE=idx;
        if(idx==BLOODRHTYPE_PLUS){
            lbl_bloodType.text = [lbl_bloodType.text stringByAppendingFormat:@"%@ +", [[self list] objectAtIndex:bloodType]];
            
        }
        else if(idx==BLOODRHTYPE_MINUS){
            lbl_bloodType.text = [lbl_bloodType.text stringByAppendingFormat:@"%@ -", [[self list] objectAtIndex:bloodType]];
           }
        else if(idx==BLOODRHTYPE_NONE){
            lbl_bloodType.text = [lbl_bloodType.text stringByAppendingFormat:@"%@ ", [[self list] objectAtIndex:bloodType]];
            NSLog(@"lbl blood type %@",lbl_bloodType.text);

        }
        
    }];
}

- (NSArray *)list
{
    return [NSArray arrayWithObjects:@"O", @"A", @"B", @"AB", nil];
}

- (NSArray *)bloodtype
{
    return [NSArray arrayWithObjects:@"Positive(+)",@"Negative(-)",@"None",nil];
}


@end
