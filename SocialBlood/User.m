//
//  User.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "User.h"

@implementation User

+ (void)setUserAuthToken:(NSString *)token{
    NSUserDefaults * perf = [NSUserDefaults standardUserDefaults];
    [perf setObject:token forKey:KEY_FOR_AUTH];
    [perf synchronize];
}

+ (NSString *)getUserAuthToken{
    NSUserDefaults * perf = [NSUserDefaults standardUserDefaults];
    NSString * token = [perf objectForKey:KEY_FOR_AUTH];
    return token;
}

@end
