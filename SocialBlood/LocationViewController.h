//
//  LocationViewController.h
//  SocialBlood
//
//  Created by Nex Technology on 6/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface LocationViewController : UIViewController
- (IBAction)onClickBack:(id)sender;
- (IBAction)getCurrentPlace:(UIButton *)sender;
@end
