//
//  LoginViewController.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "LoginViewController.h"
#import "MAFormViewController.h"
#import "MAFormField.h"
#import "User.h"

static CGFloat const kTextFieldVerticalPadding = 7.0;
static CGFloat const kTextFieldHorizontalPadding = 10.0;
static CGFloat const kToolbarHeight = 50.0;
static CGFloat const kPlaceHolderLabelFontSize = 14;
static CGFloat const kPlaceholderLabelFrameHeight = 18;
static CGFloat const kPlaceholderLabelAnimationDuration = 0.3;
static CGFloat const kDefaultSuggestedHeight = 66;
static CGFloat const kHeightIfUsingAnimatedPlaceholder = 55;

@interface LoginViewController (){

    UILabel * _placeholderLabel;
    BOOL _animatePlaceholder;
    
    IBOutlet UITextField * txtEmail;
    IBOutlet UITextField * txtPassword;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction) onSignIn:(UIButton*)sender{

    NSString * strTitle = @"Validate Error";
    if (![Utility validateEmailWithString:txtEmail.text] || [Utility stringIsEmpty:txtEmail.text shouldCleanWhiteSpace:YES]) {
        [Utility showAlert:strTitle message:@"Please enter valid email address!"];
        return;
    }
    if ([Utility stringIsEmpty:txtPassword.text shouldCleanWhiteSpace:YES] || txtPassword.text.length < 8) {
        [Utility showAlert:strTitle message:@"Your password must be at least 8 characters."];
        return;
    }
    [self requestLogin];
}

- (IBAction) onSignup:(UIButton*)sender{
    [self showForm];
}

- (IBAction) onFacebookLogin:(UIButton*)sender{
    
}

- (void)showForm {
    // create the cells
    MAFormField *name = [MAFormField fieldWithKey:@"name" type:MATextFieldTypeDefault initialValue:nil placeholder:@"Name" required:YES];
    MAFormField *email = [MAFormField fieldWithKey:@"email" type:MATextFieldTypeEmail initialValue:nil placeholder:@"Email" required:YES];
    MAFormField *password = [MAFormField fieldWithKey:@"password" type:MATextFieldTypePassword initialValue:nil placeholder:@"Password" required:YES];
    MAFormField *comfirm_password = [MAFormField fieldWithKey:@"comfirm_password" type:MATextFieldTypePassword initialValue:nil placeholder:@"Confirm Password" required:YES];
    MAFormField *location = [MAFormField fieldWithKey:@"location" type:MATextFieldTypeDefault initialValue:nil placeholder:@"Location" required:YES];
    MAFormField *phone = [MAFormField fieldWithKey:@"phone" type:MATextFieldTypePhone initialValue:nil placeholder:@"Phone" required:YES];
    MAFormField *pet = [MAFormField fieldWithKey:@"pet" type:MATextFieldTypeDefault initialValue:nil placeholder:@"Do you have any pets?" required:YES];
    MAFormField *about = [MAFormField fieldWithKey:@"about" type:MATextFieldTypeDefault initialValue:nil placeholder:@"About (Optional)" required:YES];
    MAFormField *disabledField = [MAFormField fieldWithKey:@"disabled" type:MATextFieldTypeNonEditable initialValue:@"This is not editable." placeholder:@"Disabled Field" required:NO];
    
    // separate the cells into sections
    NSArray *firstSection = @[name, email,password,comfirm_password,location, phone, pet, about];
    //NSArray *secondSection = @[location, phone, pet, about];
    //NSArray *thirdSection = @[ disabledField];
    NSArray *cellConfig = @[firstSection];
    
    // create the form, wrap it in a navigation controller, and present it modally
    MAFormViewController *formVC = [[MAFormViewController alloc] initWithCellConfigurations:cellConfig actionText:@"Sign Up" animatePlaceholders:YES handler:^(NSDictionary *resultDictionary) {
        // now that we're done, dismiss the form
        [self dismissViewControllerAnimated:YES completion:nil];
        
        // if we don't have a result dictionary, the user cancelled, rather than submitted the form
        if (!resultDictionary) {
            return;
        }
        
        // do whatever you want with the results - you can access specific values from the dictionary using
        // the key you provided when you created the form
        [[[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"Thanks for registering %@!", resultDictionary[@"name"]] delegate:nil cancelButtonTitle:@"Yay!" otherButtonTitles:nil] show];
        NSLog(@"%@", [resultDictionary description]);
    }];
    
   /* [formVC setTitleForHeaderInSectionBlock:^NSString *(NSInteger section) {
        if (section == 1) {
            return @"Address";
        }
        
        else {
            return nil;
        }
    }];
    
    [formVC setTitleForFooterInSectionBlock:^NSString *(NSInteger section) {
        if (section == 2) {
            return @"Example Footer";
        }
        
        else {
            return nil;
        }
    }];*/
    
    UINavigationController *formNC = [[UINavigationController alloc] initWithRootViewController:formVC];
    [self presentViewController:formNC animated:YES completion:nil];
}

#pragma mark request

- (void) requestLogin{

    [SVProgressHUD show];
    [SocialBloodClient signInUserWithEmail:txtEmail.text password:txtPassword.text CompletionBlock:^(NSDictionary *results) {
        NSLog(@"signin result : %@",results);
        [SVProgressHUD dismiss];
        if([Utility isNetworkReturnSuccess:results]){
            
            NSDictionary * dics = [results objectForKey:KEY_FOR_RESPONSE];
            NSString * authtoken=[dics objectForKey:@"auth_token"];
            [User setUserAuthToken:authtoken];
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }
        else{
            
            NSDictionary * dics = [results objectForKey:KEY_FOR_RESPONSE];
            NSString * msg=[dics objectForKey:@"message"];
            [Utility showAlert:@"Message" message:msg];
        }

    } andFailureBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"signin error : %@",error.description);
    }];
}

#pragma mark - placeholder animation

- (void)animatePlaceholderAbove:(UITextField*)txtField{
    //_placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTextFieldHorizontalPadding, kTextFieldVerticalPadding, CGRectGetWidth(txtField.frame) - (2 * kTextFieldHorizontalPadding), 66 - (2 * kTextFieldVerticalPadding))];
    _placeholderLabel=[[UILabel alloc]initWithFrame:CGRectMake(kTextFieldHorizontalPadding, txtField.frame.origin.y-txtField.frame.size.height,CGRectGetWidth(txtField.frame) - (2 * kTextFieldHorizontalPadding), 66 - (2 * kTextFieldVerticalPadding))];
    _placeholderLabel.text = txtField.placeholder;
    _placeholderLabel.font = [UIFont systemFontOfSize:kPlaceHolderLabelFontSize];
    _placeholderLabel.textColor = [UIColor lightGrayColor];
    _placeholderLabel.alpha = 0.0;
    
    [self.view addSubview:_placeholderLabel];
    CGRect originalFrame = _placeholderLabel.frame;
    
    [UIView animateWithDuration:kPlaceholderLabelAnimationDuration animations:^{
        _placeholderLabel.frame = CGRectMake(CGRectGetMinX(originalFrame), CGRectGetMinY(originalFrame) - 4, CGRectGetWidth(originalFrame), kPlaceholderLabelFrameHeight);
        _placeholderLabel.textColor = [UIColor redColor];
        _placeholderLabel.alpha = 1.0;
    }];
}

- (void)animatePlaceholderBack :(UITextField*)txtField{
    [UIView animateWithDuration:kPlaceholderLabelAnimationDuration animations:^{
        _placeholderLabel.frame = CGRectMake(kTextFieldHorizontalPadding, kTextFieldVerticalPadding, CGRectGetWidth(txtField.frame) - (2 * kTextFieldHorizontalPadding), 66 - (2 * kTextFieldVerticalPadding));
        _placeholderLabel.textColor = [UIColor lightGrayColor];
        _placeholderLabel.alpha = 0.0;
    } completion:^(BOOL finished) {
        [_placeholderLabel removeFromSuperview];
        _placeholderLabel = nil;
    }];
}

#pragma mark - Text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    // flag to determine if we are going to want to change characters in range
    BOOL shouldAllowEditing = YES;
    
   
    // if we're going to allow edits to be made
    if (shouldAllowEditing) {
        // tell our delegate that there have been edits made to the form
        //[self.delegate markFormHasBeenEdited];
        
        // if we want to animate the placeholder
        //if (_animatePlaceholder) {
            // animate it if we're adding characters and we don't already have a placeholder label
            if (!_placeholderLabel && resultString.length > 0) {
                [self animatePlaceholderAbove:textField];
            }
            
            // animate it back if we have a placeholder label and the resulting string will be empty
            else if (_placeholderLabel && resultString.length == 0) {
                [self animatePlaceholderBack:textField];
            }
        //}
    }
    
    return shouldAllowEditing;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // when we start editing again, update the placeholder label color to the active state
    if (_placeholderLabel) {
        _placeholderLabel.textColor = [UIColor redColor];
    }
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // when we end editing, update the placeholder label color to the inactive state
    if (_placeholderLabel) {
        _placeholderLabel.textColor = [UIColor lightGrayColor];
    }
    
    return YES;
}





@end
