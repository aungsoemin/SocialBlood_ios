//
//  HomeViewController.h
//  SocialBlood
//
//  Created by Nex Technology on 5/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+expanded.h"

@interface HomeViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel * lbl_bloodType;
@property (nonatomic, strong) IBOutlet UIView * cardView;

@end
