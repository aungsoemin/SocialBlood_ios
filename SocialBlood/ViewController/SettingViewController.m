//
//  SettingViewController.m
//  SocialBlood
//
//  Created by Thazin Nwe on 4/7/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController (){

    NSArray * arr;
    IBOutlet UITableView * tblView;
    
    IBOutlet UILabel * lblName;
    IBOutlet UILabel * lblPhone;
    IBOutlet UIImageView * imgProfile;
}

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arr= @[@"Blood Type",@"Phone Number",@"Location",@"Date of Birth",@"Gender",@"Pet",@"Email"];
    //[self createTableViewHeader];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) createTableViewHeader{
    
    float width=100;
    UIView * view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel * lblBlood =[[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-width)/2, 50, width, width)];
    lblBlood.layer.cornerRadius=width/2;
    lblBlood.layer.masksToBounds=YES;
    lblBlood.backgroundColor=[UIColor redColor];
    lblBlood.textColor=[UIColor whiteColor];
    lblBlood.text=@"A";
    lblBlood.textAlignment=NSTextAlignmentCenter;
    lblBlood.font=[UIFont fontWithName:@"Helvetica Neue" size:30];
    [view addSubview:lblBlood];
    
    UIButton * btnSelectBlood=[UIButton buttonWithType:UIButtonTypeSystem];
    [btnSelectBlood setFrame:CGRectMake((self.view.frame.size.width-width)/2, 20, width, width)];
    [btnSelectBlood addTarget:self action:@selector(onSelect:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnSelectBlood];
    
    tblView.tableHeaderView=view;
    
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [arr objectAtIndex:[indexPath row]];
    
    
    return cell;

}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
