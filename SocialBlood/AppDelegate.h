//
//  AppDelegate.h
//  SocialBlood
//
//  Created by Thazin Nwe on 4/4/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

