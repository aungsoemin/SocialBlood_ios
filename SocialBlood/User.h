//
//  User.h
//  SocialBlood
//
//  Created by Thazin Nwe on 4/5/16.
//  Copyright © 2016 Nex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * password;
@property (nonatomic, strong) NSString * comfirmPassword;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * phoneNo;
@property (nonatomic, strong) NSString * birthDate;
@property NSInteger bloodTypeId;
@property NSInteger bloodRhTypeId;
@property (nonatomic, strong) NSString * address;
@property double latitude;
@property double longitude;
@property (nonatomic, strong) NSString * about;
@property NSInteger hasPet;
@property (nonatomic, strong) NSString * pet;

+ (void)setUserAuthToken:(NSString *)token;
+ (NSString *)getUserAuthToken;

@end
